var express = require("express");
var cors = require("cors");
const mysql = require("mysql2");
const PORT = process.env.PORT || 5000;

const connection = mysql.createConnection({
  host: "157.245.59.56",
  port: "3366",
  user: "6301515",
  password: "6301515",
  database: "6301515",
});

var app = express();
app.use(cors());
app.use(express.json());

app.get("/coffee", function (req, res, next) {
  connection.query("SELECT * FROM `coffee`", function (err, results, fields) {
    res.json(results);
  });
});

app.get("/coffee/:id", function (req, res, next) {
  const id = req.params.id;
  connection.query(
    "SELECT * FROM `coffee` WHERE `id` = ?",
    [id],
    function (err, results) {
      res.json(results);
    }
  );
});

app.post("/coffee", function (req, res, next) {
  connection.query(
    "INSERT INTO `coffee`(`nname`, `hot`, `cold`, `price`, `avatar`) VALUES (?, ?, ?, ?, ?)",
    [
      req.body.nname,
      req.body.hot,
      req.body.cold,
      req.body.price,
      req.body.avatar,
    ],
    function (err, results) {
      res.json(results);
    }
  );
});

app.put("/coffee", function (req, res, next) {
  connection.query(
    "UPDATE `coffee` SET `nname`= ?, `hot`= ?, `cold`= ?, `price`= ?, `avatar`= ? WHERE id = ?",
    [
      req.body.nname,
      req.body.hot,
      req.body.cold,
      req.body.price,
      req.body.avatar,
      req.body.id,
    ],
    function (err, results) {
      res.json(results);
    }
  );
});

app.delete("/coffee", function (req, res, next) {
  connection.query(
    "DELETE FROM `coffee` WHERE id = ?",
    [req.body.id],
    function (err, results) {
      res.json(results);
    }
  );
});

app.listen(PORT, function () {
  console.log("CORS-enabled web server listening on port '+PORT");
});
